import { PolkaDotPattern } from "@lgv/pattern-polka-dot";
import { ActivityCalendar } from "@lgv/activity-calendar";

import { configurationPattern } from "../configuration.js";

/**
 * AC is a visualization of a activity over time.
 * @param {object} data - object where keys are a series item and corresponding values are objects where each is a series key/value pair.
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class AC extends ActivityCalendar {
    constructor(data, width, height, date_start, date_end) {
        super(data, width, height, null, date_start, date_end);
    }

    /**
     * Add patterns into SVG artboard.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generatePatterns() {

        // values
        let unit = this.unit;
        let ab = document.querySelector(`.${this.name}`);

        // update pattern configs
        let dot = configurationPattern.dot;
        dot.density = 8;
        dot.radius = unit / dot.density;
        let hatch = configurationPattern.hatch;
        let hBottom = configurationPattern.hBottom;
        let hTop = configurationPattern.hTop;

        // loop through each bin (threshold)
        this.threshold.range().forEach(d => {

            // dot
            let dt = new PolkaDotPattern(unit);
            dt.generate(ab, `ac-${dot.class}-${d}`, dot.radius, dot.density, dot.class);

        });

    }

};

export { AC };
export default AC;
