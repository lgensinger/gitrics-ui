import { HatchPattern } from "@lgv/pattern-hatch";
import { PolkaDotPattern } from "@lgv/pattern-polka-dot";
import { StackedBarChart } from "@lgv/stacked-bar-chart";

import { configurationPattern } from "../configuration.js";

/**
 * SBC is a visualization of a series of stacked bar charts.
 * @param {object} data - object where keys are a series item and corresponding values are objects where each is a series key/value pair.
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class SBC extends StackedBarChart {
    constructor(data, width, height, padding) {
        super(data, width, height, null, false, padding, true);
    }

    /**
     * Add patterns into SVG artboard.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generatePatterns() {

        // values
        let unit = this.unit;
        let ab = document.querySelector(`.${this.name}`);

        // update pattern configs
        let dot = configurationPattern.dot;
        dot.radius = unit / 16;
        let hatch = configurationPattern.hatch;
        let hBottom = configurationPattern.hBottom;
        let hTop = configurationPattern.hTop;

        // dot
        let d = new PolkaDotPattern(unit);
        d.generate(ab, `sbc-${dot.class}`, dot.radius, dot.density, dot.class);

        // hatch
        let h = new HatchPattern(unit);
        h.generate(ab, `sbc-${hBottom.class}`, hBottom.direction);
        h.generate(ab, `sbc-${hTop.class}`, hTop.direction);

    }

};

export { SBC };
export default SBC;
