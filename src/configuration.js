const configuration = {
    colorAccent: process.env.VUE_APP_COLOR_ACCENT || null,
    colors: process.env.VUE_APP_GITRICS_COLORS ? process.env.VUE_APP_GITRICS_COLORS.split(",").map(d => d.trim()) : null,

    condaUrl: process.env.VUE_APP_GITRICS_CONDA_URL ? process.env.VUE_APP_GITRICS_CONDA_URL : null,
    docsUrl: process.env.VUE_APP_GITRICS_DOCS_URL ? process.env.VUE_APP_GITRICS_DOCS_URL : null,
    gitlabUrl: process.env.VUE_APP_GITRICS_GITLAB_URL ? process.env.VUE_APP_GITRICS_GITLAB_URL : null,
    pypiUrl: process.env.VUE_APP_GITRICS_PYPI_URL ? process.env.VUE_APP_GITRICS_PYPI_URL : null

};

const configurationPattern = {
    dot: {
        class: "dot",
        density: 4
    },
    hatch: {
        class: "hatch"
    },
    hBottom: {
        class: "hatch-bottom-to-top",
        direction:
        "bottom"
    },
    hTop: {
        class: "hatch-top-to-bottom",
        direction:
        "top"
    },
    ring: {
        class: "ring",
        density: 5
    }
}

export { configuration, configurationPattern };
export default configuration;
