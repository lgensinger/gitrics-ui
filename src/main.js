import Vue from "vue";
import VueMeta from "vue-meta";

// material-vue
import { MdAvatar, MdButton, MdDialog } from "vue-material/dist/components";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";

// app
import App from "./App.vue";
import "./style/index.scss";

// set as vue component
Vue.use(MdAvatar);
Vue.use(MdButton);
Vue.use(MdDialog);
Vue.use(VueMeta);

// render app and mount to DOM
new Vue({
  render: h => h(App)
}).$mount("#app");
